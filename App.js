import React, { Component } from 'react'
import {
  Image,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Text,
  View, Animated,
  Dimensions, PanResponder, ActivityIndicator
} from 'react-native'
import { StackNavigator } from 'react-navigation'
import YouTube from 'react-native-youtube'
import Thumbnail from "./thumbnail.jpg";
import ChannelIcon from "./icon.png";
import Icon from 'react-native-vector-icons/MaterialIcons'
import Icons from 'react-native-vector-icons/MaterialCommunityIcons'
import Icon2 from "react-native-vector-icons/FontAwesome";
const apiKey = 'AIzaSyBJ3ntReiv0L19H2RoYW62LpRdIuyPhIpw'
const channelId = 'UCQzdMyuz0Lf4zo4uGcEujFw'
const results = 30

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      youtubeId: '',
      isLoading: false,
    }
  }
  componentDidMount() {
    this.setState({isLoading: true})
    fetch(`https://www.googleapis.com/youtube/v3/search/?key=${apiKey}&channelId=${channelId}&part=snippet,id&order=date&maxResults=${results}`)
      .then(res => res.json())
      .then(res => {
        const videoId = []
        res.items.forEach(item => {
          videoId.push(item)
        })
        this.setState({
          data: videoId,
          isLoading: false
        })
      })
      .catch(error => {
        console.error(error)
      })
  }
  componentWillMount() {
    this._y = 0
    this._animation = new Animated.Value(0)
    this._animation.addListener(({ value }) => {
      this._y = value
    })
    this._panResponder = PanResponder.create({
      onStartShouldSetPanResponder: () => true,
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([
        null,
        {
          dy: this._animation,
        }
      ]),
      onPanResponderRelease: (e, gestureState) => {
        console.log(gestureState.dy, gestureState.dx)
        if (gestureState.dy > 100) {
          Animated.timing(this._animation, {
            toValue: 300,
            duration: 200,
          }).start();
          this._animation.setOffset(300)
        } else {
          this._animation.setOffset(0);
          Animated.timing(this._animation, {
            toValue: 0,
            duration: 200,
          }).start();
        }
      }

    })
  }
  handleOpen = (youtubeid) => {
    this.setState({
      youtubeId: youtubeid
    })
    this._animation.setOffset(0);
    Animated.timing(this._animation, {
      toValue: 0,
      duration: 200,
    }).start();
  }
  render() {
    const { width, height: screenHeight } = Dimensions.get("window");
    const height = width * 0.5625;
    const opacityInterpolate = this._animation.interpolate({
      inputRange: [0, 300],
      outputRange: [1, 0],
    });
    const translateYInterpolate = this._animation.interpolate({
      inputRange: [0, 300],
      outputRange: [0, (screenHeight) - height - 36],
      extrapolate: "clamp",
    })
    const translateXInterpolate = this._animation.interpolate({
      inputRange: [0, 300],
      outputRange: [0, 68],
      extrapolate: "clamp",
    });
    const scaleYInterpolate = this._animation.interpolate({
      inputRange: [0, 300],
      outputRange: [1, 0.6],
      extrapolate: "clamp",
    })
    const scaleXInterpolate = this._animation.interpolate({
      inputRange: [0, 300],
      outputRange: [1, 0.6],
      extrapolate: "clamp",
    })
    const scrollScale = this._animation.interpolate({
      inputRange: [0, 300],
      outputRange: [1, 0],
      extrapolate: "clamp",
    });
    const scrollStyles = {
      opacity: opacityInterpolate,
      transform: [
        {
          translateY: translateYInterpolate,
        },
      ],
    };
    const videoStyles = {
      transform: [
        {
          translateY: translateYInterpolate,
        },
        {
          translateX: translateXInterpolate,
        },
        {
          scaleY: scaleYInterpolate,
        },
        {
          scaleX: scaleXInterpolate,
        },
      ]
    }
    const tabBottom = this._animation.interpolate({
      inputRange: [0, 300],
      outputRange: [-48, 0],
      extrapolate: 'clamp'
    });
    const { navigate } = this.props.navigation
    return (
      <View style={styles.container}>

        <ScrollView style={{ flex: 1 }}>
          <Animated.View style={{
            position: 'absolute',
            top: 0,
            left: 0,
            right: 0,
            backgroundColor: '#fff',
            justifyContent: 'space-around',
            height: 48,
            flexDirection: 'row',
            alignItems: 'center',
            elevation: 4,
          }}>
            <View style={{ justifyContent: 'space-around', flex: 0.4 }}>
              <TouchableOpacity>
                <Animated.Image
                  style={{ height: 22, width: 98, marginLeft: 15 }}
                  source={require('./images/logo.png')} />
              </TouchableOpacity>
            </View>

            <View style={{
              flexDirection: 'row',
              justifyContent: 'flex-end', flex: 0.6, alignItems: 'center',
            }}>
              <TouchableOpacity style={{ padding: 12 }}>
                <Icon name='videocam' size={24} color={'#555'} />
              </TouchableOpacity>
              <TouchableOpacity style={{ padding: 12 }}>
                <Icon name='search' size={24} color={'#555'} />
              </TouchableOpacity>
              <TouchableOpacity style={{ padding: 12 }}>
                <Icon name='account-circle' size={24} color={'#555'} />
              </TouchableOpacity>
            </View>
          </Animated.View>
          <View style={styles.body}>
            {this.state.isLoading ?
              <View style={{
                flex: 1,
                height: Dimensions.get("window").height - 56,
                justifyContent: 'center', alignItems: 'center'
              }}>
                <ActivityIndicator color='grey' size='large' />
              </View> :
              this.state.data.map((item, i) =>
                <TouchableHighlight
                  key={item.id.videoId}
                  onPress={() => this.handleOpen(item.id.videoId)}>
                  {/* onPress={() => this.props.navigation.navigate('YoutubeVideo', {youtubeId: item.id.videoId})}> */}
                  <View style={styles.vids}>
                    <Image
                      source={{ uri: item.snippet.thumbnails.medium.url }}
                      style={{ width: 328, height: 180 }} />
                    <View style={styles.vidItems}>

                      <Image
                        source={require('./images/NightKing.jpg')}
                        style={{ width: 40, height: 40, borderRadius: 20, marginRight: 5, padding: 10 }} />
                      <Text style={styles.vidText}>{item.snippet.title}</Text>

                      <TouchableOpacity style={{ padding: 5, borderRadius: 15 }}>
                        <Icon name='more-vert' size={20} color='#555' />
                      </TouchableOpacity>
                    </View>
                  </View>
                </TouchableHighlight>
              )}
          </View>

        </ScrollView>
        <Animated.View style={[styles.tabBar, { bottom: this.state.youtubeId != '' ? tabBottom : 0 }]}>
          <TouchableOpacity style={[styles.tabItems]}>
            <Icon name='home' size={22} color='#444' />
            <Text style={styles.tabTitle}>Home</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='whatshot' size={22} color='#444' />
            <Text style={styles.tabTitle}>Trending</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='subscriptions' size={22} color='#444' />
            <Text style={styles.tabTitle}>Subscriptions</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icons name='bell' size={22} color='#444' />
            <Text style={styles.tabTitle}>Activity</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItems}>
            <Icon name='folder' size={22} color='#444' />
            <Text style={styles.tabTitle}>Library</Text>
          </TouchableOpacity>
        </Animated.View>
        {
          this.state.youtubeId != '' ?
            <View style={StyleSheet.absoluteFill} pointerEvents="box-none">

              <Animated.View style={[{ width, height }, videoStyles]} {...this._panResponder.panHandlers}>
                <YouTube
                  videoId={this.state.youtubeId}
                  play={true}
                  fullscreen={false}
                  loop={false}
                  apiKey={'AIzaSyC6P862Sa4GqumBYvyyNJAtOyRB_A0Kf6U'}
                  onReady={e => this.setState({ isReady: true })}
                  onChangeState={e => this.setState({ status: e.state })}
                  onChangeQuality={e => this.setState({ quality: e.quality })}
                  onError={e => this.setState({ error: e.error })}
                  repeat style={StyleSheet.absoluteFill} resizeMode="contain"
                  
                />
              </Animated.View>
              <Animated.ScrollView
                style={[styles.scrollView, scrollStyles]}
                onLayout={(event) => { this.find_dimesions(event.nativeEvent.layout) }}>
                <View style={[styles.topContent, styles.padding]}>
                  <Text style={styles.title}>Beautiful DJ Mixing Lights</Text>
                  <Text>1M Views</Text>
                  <View style={styles.likeRow}>
                    <TouchableIcon name="thumbs-up">10,000</TouchableIcon>
                    <TouchableIcon name="thumbs-down">3</TouchableIcon>
                    <TouchableIcon name="share">Share</TouchableIcon>
                    <TouchableIcon name="download">Save</TouchableIcon>
                    <TouchableIcon name="plus">Add to</TouchableIcon>
                  </View>
                </View>

                <View style={[styles.channelInfo, styles.padding]}>
                  <Image
                    source={ChannelIcon}
                    style={styles.channelIcon}
                    resizeMode="contain"
                  />
                  <View style={styles.channelText}>
                    <Text style={styles.channelTitle}>Prerecorded MP3s</Text>
                    <Text>1M Subscribers</Text>
                  </View>
                </View>

                <View style={styles.padding}>
                  <Text style={styles.playlistUpNext}>Up next</Text>
                  <PlaylistVideo
                    image={Thumbnail}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                  />
                  <Text style={styles.playlistUpNext}>Up next</Text>
                  <PlaylistVideo
                    image={Thumbnail}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                  />
                  <Text style={styles.playlistUpNext}>Up next</Text>
                  <PlaylistVideo
                    image={Thumbnail}
                    name="Next Sweet DJ Video"
                    channel="Prerecorded MP3s"
                    views="380K"
                  />

                </View>

              </Animated.ScrollView>

            </View> :
            <View />
        }
      </View>
    )
  }
}
const PlaylistVideo = ({ name, channel, views, image }) => {
  return (
    <View style={styles.playlistVideo}>
      <Image source={image} style={styles.playlistThumbnail} resizeMode="cover" />
      <View style={styles.playlistText}>
        <Text style={styles.playlistVideoTitle}>
          {name}
        </Text>
        <Text style={styles.playlistSubText}>
          {channel}
        </Text>
        <Text style={styles.playlistSubText}>
          {views} views
        </Text>
      </View>
    </View>
  );
};
const TouchableIcon = ({ name, children }) => {
  return (
    <TouchableOpacity style={styles.touchIcon}>
      <Icon2 name={name} size={30} color="#767577" />
      <Text style={styles.iconText}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  body: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 52,
  },
  vids: {
    width: Dimensions.get('window').width,
    paddingTop: 20,
    paddingBottom: 30,
    alignItems: 'center',
    backgroundColor: '#fff',
    justifyContent: 'space-around',
    borderBottomWidth: 0.6,
    borderColor: '#aaa'
  },
  vidItems: {
    padding: 20,
    paddingRight: 10,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',

  },
  vidText: {
    width: 270,
    alignItems: 'center',
    justifyContent: 'space-around',
    color: '#000'
  },
  tabBar: {
    position: 'absolute',
    left: 0,
    right: 0,
    backgroundColor: '#fff',
    height: 48,
    flexDirection: 'row',
    justifyContent: 'space-around',
    elevation: 6,
  },
  tabItems: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 0
  },
  tabTitle: {
    fontSize: 11,
    color: '#333',
    paddingTop: 0,
    textDecorationLine: 'none'
  },
  scrollView: {
    flex: 1,
    backgroundColor: "#FFF",
  },
  touchIcon: {
    alignItems: "center",
    justifyContent: "center",
  },
  iconText: {
    marginTop: 5,
  },
  title: {
    fontSize: 28,
  },
  likeRow: {
    flexDirection: "row",
    justifyContent: "space-around",
    paddingVertical: 15,
  },
  padding: {
    paddingVertical: 15,
    paddingHorizontal: 15,
  },
  channelInfo: {
    flexDirection: "row",
    borderBottomWidth: 1,
    borderBottomColor: "#DDD",
    borderTopWidth: 1,
    borderTopColor: "#DDD",
  },
  channelIcon: {
    width: 50,
    height: 50,
  },
  channelText: {
    marginLeft: 15,
  },
  channelTitle: {
    fontSize: 18,
    marginBottom: 5,
  },
  playlistUpNext: {
    fontSize: 24,
  },
  playlistVideo: {
    flexDirection: "row",
    height: 100,
    marginTop: 15,
    marginBottom: 15,
  },
  playlistThumbnail: {
    width: null,
    height: null,
    flex: 1,
  },
  playlistText: {
    flex: 2,
    paddingLeft: 15,
  },
  playlistVideoTitle: {
    fontSize: 18,
  },
  playlistSubText: {
    color: "#555",
  },
})

export default screens = StackNavigator({
  Home: { screen: App }
}, { headerMode: 'none' })
